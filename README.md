# vscode-tao

A [Tao][tao] syntax extension for VSCode.

[tao]: https://github.com/zesterer/tao

## Features

Syntax highlighting!

If you find any problems or things that need some work,
feel free to open an [issue][issues] or [merge request][pulls]!

[issues]: https://gitlab.com/wackbyte/vscode-tao/-/issues
[pulls]: https://gitlab.com/wackbyte/vscode-tao/-/merge_requests

## Disclaimer

I am not involved in the development of Tao.
However, I will try my best to keep the grammar up to date as it evolves.

## License

I release this extension into the public domain under the [Unlicense](UNLICENSE).
