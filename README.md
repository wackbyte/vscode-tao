# vscode-tao

A [Tao](https://github.com/zesterer/tao) syntax extension for VS Code.

## Features

Syntax highlighting!

If you find any problems or things that need some work,
feel free to open an [issue][issues] or [pull request][pulls]!

[issues]: https://codeberg.org/wackbyte/vscode-tao/issues
[pulls]: https://codeberg.org/wackbyte/vscode-tao/pulls

## Disclaimer

I am not involved in the development of Tao.
However, I will try my best to keep the grammar up to date as it evolves.

## License

I release this extension into the public domain under the [Unlicense](UNLICENSE).
