# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog],
and this project adheres to [Semantic Versioning].

## [Unreleased]

### Added

- The `mod` keyword.

### Changed

- Highlight `:` as an operator instead of punctuation.

## [0.0.13]

Released on 2023-02-09.

### Fixed

- Add missing `version` to the Nix derivation.

## [0.0.12]

Released on 2023-02-08.

### Added

- `changelog` and `downloadPage` information to the Nix derivation.
- The `when` and `is` keywords.

### Changed

- Internal cleanup.
- Don't try to match for a sign (`-`) in integer and real literals.

### Fixed

- Add missing `vscodeExtPublisher` and `vscodeExtName` to the Nix derivation.
- Properly match character escapes.

## [0.0.11]

Released on 2022-07-14.

### Added

- The `as` keyword.
- The [Keep a Changelog] message to this changelog.

### Changed

- Make `import` and `where` declaration keywords.
- The `!` comment to indicate it also represents the propogation operator.

### Removed

- The `@{` syntax.
- Boolean literals (`True` and `False`).
- The `Bool` primitive.

## [0.0.10]

Released on 2022-04-10.

### Added

- Highlight `@{`, the current block syntax, specially.
- The `effect`, `handle`, `return`, `where`, and `with` keywords.

### Changed

- Highlight the `~` and `..` tokens as punctuation instead of operators.

## [0.0.9]

Released on 2022-03-18.

### Fixed

- Intrinsics take precedence over types and variables and are highlighted properly again.

## [0.0.8]

Released on 2022-03-15.

### Added

- `@`, the universe type.

### Changed

- `<-` is highlighted as an operator instead of punctuation.
- `fn` is a declaration keyword.

## [0.0.7]

Released on 2022-02-23.

### Added

- Type suffixes for integers, like `1i` or `23u`.

### Changed

- Integer literals are matched separately from real literals.
- Move the integer types `Nat`, `Int`, and `Real` out of `entity.name.type.primitive` and into `entity.name.type.numeric`.

### Fixed

- Real literals must have at least one digit after the dot (this is Tao's behavior).

## [0.0.6]

Released on 2022-02-22.

### Added

- Support for intrinsics, such as `@type_name`.

### Fixed

- Variables can no longer start with uppercase letters since those are reserved for type names.

## [0.0.5]

Released on 2022-02-13.

### Changed

- Move the `#attribute` pattern to the `repository`.
- Move operators out of the `#keyword` pattern and into the `#operator` pattern.
- Move the pattern operators `~` and `..` from the `keyword.operator` namespace to `keyword.operator.pattern`.

### Fixed

- Variables are highlighted properly when beginning with an underscore.

## [0.0.4]

Released on 2022-02-13.

### Added

- Support for the unknown type `?`.

## [0.0.3]

Released on 2022-02-13.

### Added

- Highlighting for type names (identifiers beginning with capital letters).
- The `Self` type.

### Changed

- Turn the `#primitive` pattern into the `#type` pattern, which also contains the above additions.

## [0.0.2]

Released on 2022-02-11.

### Changed

- Rename `Num` to `Real`.

## [0.0.1]

Released on 2022-01-04.

### Added

- Initial syntax highlighting support.

[Unreleased]: https://gitlab.com/wackbyte/vscode-tao/-/compare/v0.0.13...trunk
[0.0.13]: https://gitlab.com/wackbyte/vscode-tao/-/compare/v0.0.12...v0.0.13
[0.0.12]: https://gitlab.com/wackbyte/vscode-tao/-/compare/v0.0.11...v0.0.12
[0.0.11]: https://gitlab.com/wackbyte/vscode-tao/-/compare/v0.0.10...v0.0.11
[0.0.10]: https://gitlab.com/wackbyte/vscode-tao/-/compare/v0.0.9...v0.0.10
[0.0.9]: https://gitlab.com/wackbyte/vscode-tao/-/compare/v0.0.8...v0.0.9
[0.0.8]: https://gitlab.com/wackbyte/vscode-tao/-/compare/v0.0.7...v0.0.8
[0.0.7]: https://gitlab.com/wackbyte/vscode-tao/-/compare/v0.0.6...v0.0.7
[0.0.6]: https://gitlab.com/wackbyte/vscode-tao/-/compare/v0.0.5...v0.0.6
[0.0.5]: https://gitlab.com/wackbyte/vscode-tao/-/compare/v0.0.4...v0.0.5
[0.0.4]: https://gitlab.com/wackbyte/vscode-tao/-/compare/v0.0.3...v0.0.4
[0.0.3]: https://gitlab.com/wackbyte/vscode-tao/-/compare/v0.0.2...v0.0.3
[0.0.2]: https://gitlab.com/wackbyte/vscode-tao/-/compare/v0.0.1...v0.0.2
[0.0.1]: https://gitlab.com/wackbyte/vscode-tao/-/tags/v0.0.1

[Keep a Changelog]: https://keepachangelog.com/en/1.1.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
