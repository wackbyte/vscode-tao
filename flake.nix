{
  description = "Tao syntax extension for VSCode";
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
        lib = pkgs.lib;
      in
      rec {
        defaultPackage = pkgs.vscode-utils.buildVscodeExtension rec {
          name = "vscode-tao";
          version = "0.0.13";

          src = ./.;

          vscodeExtPublisher = "wackbyte";
          vscodeExtName = "vscode-tao";
          vscodeExtUniqueId = "${vscodeExtPublisher}.${vscodeExtName}";

          meta = with lib; {
            description = "Tao syntax extension for VSCode";
            homepage = "https://gitlab.com/wackbyte/vscode-tao";
            downloadPage = "https://gitlab.com/wackbyte/vscode-tao/-/tags/v${version}";
            changelog = "https://gitlab.com/wackbyte/vscode-tao/-/raw/v${version}/CHANGELOG.md";
            license = licenses.unlicense;
            platforms = platforms.all;
          };
        };
        packages.vscode-tao = defaultPackage;
        legacyPackages.vscode-tao = defaultPackage;

        formatter = pkgs.nixpkgs-fmt;
      }
    );
}
