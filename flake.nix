{
  description = "Tao syntax extension for VS Code";
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
        lib = pkgs.lib;
      in
      rec {
        defaultPackage = pkgs.vscode-utils.buildVscodeExtension rec {
          name = "vscode-tao";
          version = "0.0.15";

          src = ./.;

          vscodeExtPublisher = "wackbyte";
          vscodeExtName = "tao";
          vscodeExtUniqueId = "${vscodeExtPublisher}.${vscodeExtName}";

          meta = with lib; {
            description = "Tao syntax extension for VS Code";
            homepage = "https://codeberg.org/wackbyte/vscode-tao";
            downloadPage = "https://codeberg.org/wackbyte/vscode-tao/releases/tag/v${version}";
            changelog = "https://codeberg.org/wackbyte/vscode-tao/raw/branch/trunk/CHANGELOG.md";
            license = licenses.unlicense;
            platforms = platforms.all;
          };
        };
        packages.vscode-tao = defaultPackage;
        legacyPackages.vscode-tao = defaultPackage;

        formatter = pkgs.nixpkgs-fmt;
      }
    );
}
